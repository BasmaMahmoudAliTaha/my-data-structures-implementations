package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;

public class SortingTechnics<T extends Comparable<T>> implements ISort <T>{

	@Override
	public IHeap<T> heapSort(ArrayList<T> unordered) {


		BinHeap<T> heap  = new BinHeap<T>();

		if(unordered == null)
			try {
				throw new Exception("Null heap!");
			} catch (Exception e1) {}

		else if(unordered.isEmpty())
			return heap;

		heap.build(unordered);
		heap.sortHeap();

		return heap;
	}

	@Override
	public void sortSlow(ArrayList<T> unordered) {
		bubbleSort(unordered);
	}

	/**Bubble sort O(n^2)**/
	private void bubbleSort(ArrayList<T> unordered){
		if(unordered.isEmpty())
			return;
		boolean swapped = true;
		int j = 0;
		int size =  unordered.size();
		while(swapped){
			swapped = false;
			j++;
			for(int i = 0; i < size-j;  i++){
				T elem1 = unordered.get(i); T elem2 = unordered.get(i+1);
				if(elem1.compareTo(elem2) > 0){
					unordered.set(i,elem2);
					unordered.set(i+1, elem1);
					swapped = true;
				}
			}
		}
	}

	@Override
	public void sortFast(ArrayList<T> unordered) {
		if(unordered.isEmpty())
			return;
		//mergeSort(unordered, 0, unordered.size()-1);
		quickSort(unordered, 0, unordered.size()-1);
	}

	/***Quick sort****/
	private void quickSort(ArrayList<T> unordered,int p, int r){

		if(p < r){
			int q = partition(unordered, p, r);
			quickSort(unordered,p, q-1);
			quickSort(unordered,q + 1, r);
		}


	}

	private int partition(ArrayList<T> unordered,int p, int r){

		T x = unordered.get(r);
		int i = p -1;
		for(int j = p; j <= r-1; j++){

			if(unordered.get(j).compareTo(x) <= 0){
				i++;
				T temp = unordered.get(i);
				unordered.set(i, unordered.get(j));
				unordered.set(j, temp);
			}
		}

		T temp = unordered.get(i+1);
		unordered.set(i+1, unordered.get(r));
		unordered.set(r, temp);

		return i + 1;
	}
	/***Merge Sort O(nlgn)***/

//	private void mergeSort(ArrayList<T> unordered, int p,int r){
//		if(p < r){
//			int q = (p+r)/2;
//			mergeSort(unordered, p, q);
//			mergeSort(unordered, q+1, r);
//			merge(unordered, p, q, r);
//		}
//	}
//
//
//	private void merge(ArrayList<T> unordered, int p,int q, int r){
//		int size1 = q - p + 1 ; int size2 = r - q ;
//		if(size1 == 0 && size2 == 0)
//			return;
//		ArrayList<T> left = new ArrayList<T>();
//		ArrayList<T> right = new ArrayList<T>();
//		int i = 0, j=0, k = 0;
//
//		for( i = 0; i < size1; i++){
//			if(i != 0)
//				left.add(unordered.get(p + i - 1));
//			else
//				left.add(unordered.get(p + i ));
//		}
//		for(j = 0; j < size2; j++){
//			right.add(unordered.get(q + j));
//		}
//		i= 0; j = 0;
//
//		for(k = p; k < r; k++){
//			if(left.get(i).compareTo(right.get(j)) <= 0){
//				unordered.set(k, left.get(i));
//				i++;
//			}
//			else{
//				unordered.set(k, right.get(j));
//				j++;
//			}
//		}
//	}
}