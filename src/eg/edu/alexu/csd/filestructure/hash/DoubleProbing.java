package eg.edu.alexu.csd.filestructure.hash;

import java.util.HashMap;
import java.util.Set;

public class DoubleProbing<K, V> implements IHashDouble, IHash<K, V> {
	
	//=================== Attributes =====================//

	private Pair<K,V>[] hashTable;
	private int collisions, capacity, size;
	private HashMap<K, Integer> keys;

	//=================== Constructor =====================//

	@SuppressWarnings("unchecked")
	public DoubleProbing(){
		collisions = 0;
		keys = new HashMap<K, Integer>();
		capacity = 1200;
		size =  0;
		hashTable = new Pair[capacity];

	}
	@SuppressWarnings("unchecked")
	@Override

	public void put(K key, V value) {

		if(key == null) {
			throw new RuntimeException("Null Key!");
		}

		if(keys.containsKey(key)) { // For duplicate keys, just update the key's value
			updateKey(key, value);
			return;
		}
		boolean inserted = map(key, value);

		if(!inserted) { // Maximum number of probing reached.
			Pair<K,V>[] hashCopy = new Pair[capacity];
			++collisions;
			System.arraycopy(hashTable, 0, hashCopy, 0, hashTable.length);
			capacity *= 2; // update the capacity
			hashTable = new Pair[capacity];
			rehash(hashCopy);
			map(key, value);
		}
		size++;
		keys.put(key, 0);
	}
	@Override
	public String get(K key) {

		if (key == null) { 
			throw new RuntimeException("Null arguement!");
		}

		if(!keys.containsKey(key) || size == 0) { // key isn't in the table
			return null;
		}

		int keyPlace = search(key);
		if(keyPlace != -1) { //  key is found
			return (String) hashTable[keyPlace].getValue();
		}
		return null;
	}

	@Override
	public void delete(K key) {

		if (key == null) { 
			throw new RuntimeException("Null arguement!");
		}
		if(!keys.containsKey(key) || size == 0) { // key isn't in the table
			return;
		}
		int keyPlace = search(key);
		if(keyPlace != -1) { //  key is found, if not found do nothing

			hashTable[keyPlace] = null;
			size--;
			keys.remove(key);

		}
	}

	@Override
	public boolean contains(K key) {
		if (key == null) { 
			throw new RuntimeException("Null arguement!");
		}
		if(!keys.containsKey(key) || size == 0) { // key isn't in the table
			return false;
		}
		return search(key) != -1 ? true : false;
	}

	@Override
	public boolean isEmpty() {

		return size == 0 ? true : false;
	}

	@Override
	public int size() {

		return size;
	}

	@Override
	public int capacity() {

		return capacity;
	}

	@Override
	public int collisions() {

		return collisions;
	}

	@Override
	public Iterable<K> keys() {
		Set<K> keySet = keys.keySet();
		return keySet;
	}

	//=============== Locally used Functions ===================//

	private boolean map(K key, V value) {
		int i = 0, h1, h2, h;
		h1 =  key.hashCode() % capacity;
	//	int R = getLargestPrimeLessThanSize();
		h2 = 1193 - (key.hashCode() % 1193);
		h = h1;
		
		while (i < capacity) {
			
			if(hashTable[h] == null) {
				hashTable[h] = new Pair<K,V>(key,value);
				return true; // inserted
			}
			else {
				++i;
				h = (h1 + i * h2) % capacity;
			}
			++collisions;
		}
		return false;
	}

	private void rehash(Pair<K, V>[] hashCopy) {

		for (int i = 0 ; i < hashCopy.length; ++i) {
			if(hashCopy[i] == null){
				continue;
			}
			map(hashCopy[i].getKey(), hashCopy[i].getValue());
		}
	}

	private void updateKey(K key, V value) {

		int keyPlace = search(key);
		hashTable[keyPlace].setValue(value);
	}

	private int search(K key) {
		int i = 0, h1, h2, h;
		h1 =  key.hashCode() % capacity;
		int R = getLargestPrimeLessThanSize();
		h2 = R - (key.hashCode() % R); h = h1;

		while (i < capacity) {

			if(hashTable[h] != null && hashTable[h].getKey().equals(key)) {
				return h; // Found, return index
			}
			else {
				++i;
				h = (h1 + i * h2) % capacity;
			}
		}
		return -1; // there is no enough rooms
	}
	
	public int getLargestPrimeLessThanSize() {
		int i;
		for(i = capacity; i > 0; --i){
			if(isPrime(i)) {
				break;
			}
		}
		return i;
	}
	
	private boolean isPrime(int x) {
		
		if (x < 2) {
			return false;
		}
        if (x == 2) {
        	return true;
        }
        if (x % 2 == 0) {
        	return false;
        }
        for (int i = 3; i * i <= x; i += 2) {
        	if (x % i == 0) {
        		return false;
        	}
        }  
        return true;
	}
}
