package eg.edu.alexu.csd.filestructure.avl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Dictionary implements IDictionary {
	private AvlTree<String> dictionaryAvl;


	public Dictionary() {

		dictionaryAvl = new AvlTree<String>(true);
	}
	@Override
	public void load(File file) {
		BufferedReader buffer = null;
		try{
			String currentLine;
			buffer = new BufferedReader(new FileReader(file));

			while((currentLine = buffer.readLine())!=null){

				dictionaryAvl.insert(currentLine);

			}

		} catch(IOException e){

			System.out.println("There is an i/o error!");

		} finally{

			try {

				if (buffer != null)
					buffer.close();

			} catch (IOException ex) {
				System.out.println("Error encountered while closing buffer!");			
			}

		}

	}

	@Override
	public boolean insert(String word) {

		if(dictionaryAvl.search(word))
			return false;

		dictionaryAvl.insert(word);
		
		return true;
	}

	@Override
	public boolean exists(String word) {

		return dictionaryAvl.search(word);
	}

	@Override
	public boolean delete(String word) {

		return dictionaryAvl.delete(word);
	}

	@Override
	public int size() {

		return dictionaryAvl.getSize();
	}

	@Override
	public int height() {

		return dictionaryAvl.height();
	}

	public AvlTree<String> getDictionaryAvl() {
		return dictionaryAvl;
	}
}
