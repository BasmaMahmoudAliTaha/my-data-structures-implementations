package eg.edu.alexu.csd.filestructure.graphs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Graph implements IGraph {
	//private BinHeap<Integer> minHeap;
	private Map<Integer, LinkedHashMap<Integer, Integer>> graph;
	private int V,E;
	private ArrayList<Integer> dijkstraProcess;

	public Graph() {
		dijkstraProcess = new ArrayList<Integer>();
		graph = new HashMap<Integer,LinkedHashMap<Integer, Integer>>();
	}

	@Override
	public void readGraph(File file) {

		BufferedReader buffer = null;
		try{
			String currentLine;
			buffer = new BufferedReader(new FileReader(file));
			currentLine = buffer.readLine();
			if(currentLine != null){
				V = Integer.parseInt(currentLine.split(" ")[0]);
				E = Integer.parseInt(currentLine.split(" ")[1]);
				int counter = 0;
				while((currentLine = buffer.readLine())!=null){
					int i = Integer.parseInt(currentLine.split(" ")[0]);
					int j = Integer.parseInt(currentLine.split(" ")[1]);
					int w = Integer.parseInt(currentLine.split(" ")[2]);
					counter++;
					if(j >= V || i < 0){
						throw new RuntimeException("Index out of bound");
					}
					LinkedHashMap<Integer, Integer> temp;
					if(graph.containsKey(i)){
						temp = graph.get(i);
						temp.put(j, w);
					}
					else{
						temp = new LinkedHashMap<Integer, Integer>();
						temp.put(j,w);
						graph.put(i, temp);
					}
				}
				if(counter > E || counter < E){
					throw new RuntimeException("Input number incorrect.");
				}
			}else{
				throw new RuntimeException("Empty file");
			}
		} catch(IOException e){
			throw new RuntimeException("File doesn't exist!");
		} finally{
			try {
				if (buffer != null)
					buffer.close();
			} catch (IOException ex) {
				throw new RuntimeException("Error encountered while closing buffer!");			
			}
		}
	}

	@Override
	public int size() {
		return E;
	}

	@Override
	public ArrayList<Integer> getVertices() {
		if(!graph.isEmpty()){
			ArrayList<Integer> vertices = new ArrayList<Integer>(graph.keySet());
			if(vertices.size() < V){
				for(int i = 0; i < V; ++i){
					if(!vertices.contains(i)){
						vertices.add(i);
					}
				}
			}
			return vertices;
		}else{
			return new ArrayList<Integer>();
		}
	}

	@Override
	public ArrayList<Integer> getNeighbors(int v) {
		ArrayList<Integer> adjacency = new ArrayList<Integer>();
		if(graph.containsKey(v)){
			adjacency = new ArrayList<>(graph.get(v).keySet());
			return adjacency;
		}
		return new ArrayList<Integer>();
	}

	@Override
	public void runDijkstra(int src, int[] distances) { // O(E log(V))
		try{
			
			BinHeap<Integer> minHeap = new BinHeap<Integer>();
			int infinity = Integer.MAX_VALUE/2;
			//Initialize heap infinity unless the source with zero
			for(int i = 0; i < distances.length; ++i){
				distances[i] = infinity;
			//	minHeap.insert(infinity,i);
			}
			// initialize src distance to zero.
			minHeap.insert(0, src);
			distances[src] = 0;
			while(minHeap.size() != 0){
				int u = minHeap.getVertexNum(0);
				minHeap.extract();
				dijkstraProcess.add(u);
				LinkedHashMap<Integer, Integer> adjecent = graph.get(u);
				if(adjecent!=null){
					for(Map.Entry<Integer, Integer> entry : adjecent.entrySet()){
						int v = entry.getKey();
						int weight = entry.getValue();
						// If shortest distance to v is not finalized yet, and distance to v
						// through u is less than its previously calculated distance
						if(weight + distances[u] < distances[v]){
							distances[v] = distances[u] + weight;
							// update distance value in min heap also
							minHeap.insert(distances[v],v);
						}
					}
				}
			}
		}catch(Exception e){
			throw new RuntimeException("Null");
		}
	}

	@Override
	public ArrayList<Integer> getDijkstraProcessedOrder() {
		return dijkstraProcess;
	}

	@Override
	public boolean runBellmanFord(int src, int[] distances) {
		try{
			
			int infinity = Integer.MAX_VALUE/2;
			for (int i=0; i<V; ++i)
				distances[i] = infinity;
			distances[src] = 0;
			
			// Step 2: Relax all edges |V| - 1 times. A simple
			// shortest path from src to any other vertex can
			// have at-most |V| - 1 edges
			for(int i = 1; i <= V-1; ++i){
				// For each edge u-v relax..
				boolean flag = false;
				for (Map.Entry<Integer, LinkedHashMap<Integer, Integer>> vertices : graph.entrySet()){
					LinkedHashMap<Integer, Integer> adj = vertices.getValue();
					int u = vertices.getKey();
					for(Map.Entry<Integer, Integer> edges : adj.entrySet()){
						/* If dist[v] > dist[u] + weight of edge uv, 
						 * then update dist[v]..
						 * dist[v] = dist[u] + weight of edge uv
						 */
						int v = edges.getKey();
						int weight = edges.getValue();
						if(distances[u] != infinity
								&& distances[v] > (distances[u] + weight)){
							distances[v] = distances[u] + weight;
							flag = true;
						}
					}	
				}
				if(!flag){
					return true;
				}	
			}
			// See if there is any negative cycles.
			for (Map.Entry<Integer, LinkedHashMap<Integer, Integer>> 
			vertices : graph.entrySet()){
				LinkedHashMap<Integer, Integer> adj = vertices.getValue();
				int u = vertices.getKey();
				for(Map.Entry<Integer, Integer> edges : adj.entrySet()){
					/* If dist[v] > dist[u] + weight of edge uv, 
					 * then update dist[v]..
					 * dist[v] = dist[u] + weight of edge uv
					 */
					int v = edges.getKey();
					int weight = edges.getValue();
					if(distances[u] != infinity &&
							distances[v] > (distances[u] + weight)){
						return false;
					}
				}	
			}
			return true;
		}catch(Exception e){
			throw new RuntimeException("Null");
		}
		
	}

}
